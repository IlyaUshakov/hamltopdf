from flask import Flask, request, send_file
import os
import uuid

app = Flask(__name__)

#base directory that all files have to reffer to
base_dir = os.path.dirname(os.path.abspath(__file__))

def haml_to_html(tmp_haml_file):
	"""
	create_html()
	Created html file from haml file, returns path to haml file
	"""
	filename = str(uuid.uuid4()) #temprorary file to store html
	tmp_html = base_dir+"/"+filename + ".html"

	os.system("haml "+tmp_haml_file+" > "+tmp_html)

	f = open(tmp_html, "r")
	html = f.read()
	f.close()
	os.remove(tmp_html)

	return html

@app.route('/convert', methods=['POST'])
def post():
	"""
	post()
	Receive haml file and responce with html file
	"""
	#save recieved file
	file = request.files['file']
	tmp_haml_file = base_dir+"/"+str(uuid.uuid4())+".haml" 
	file.save(tmp_haml_file)

	#convert
	responce = haml_to_html(tmp_haml_file)

	#delete temprorary file
	os.remove(tmp_haml_file)
	return responce

@app.route('/')
def hello():
	return "Hello to HAML to html converter"


if __name__ == '__main__':
	"""Start Flask """
	app.run(debug=False, host='0.0.0.0', port=80)