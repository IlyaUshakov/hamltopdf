.. HamlToHTML documentation master file, created by
   sphinx-quickstart on Fri Jan 18 07:01:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HamlToHTML's documentation!
======================================

Project build on python3 with Flask framework

Haml to Html
Converts Haml file -> Html file. 
Written with Flask framework

How it works internally:
################################
Downloaded haml file converts to html file, then html converts to html.

Start service:
########################

Creating image with Docker

	``./gradlew HamlToHtmlDocker``

Run docker

	``docker run -p 80:80 hamlapp``


Send file:
################
example.haml  file that contacts haml code

To output html

    ``curl -F 'file=@example.haml' http://localhost:80/convert``

To save as a file

    ``curl -F 'file=@example.haml' http://localhost:80/convert > index.html``



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
