#Had to user Ubuntu, because of puby-haml. In other cases i would use just python
FROM ubuntu:18.10 

LABEL maintainer="ilya@ushakov.me"

#installing python3
RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip ruby-haml 

#requirements contain flask. It is in a selerated file for futher usage to easyly add requirements without changing Dockerfile
ADD requirements.txt /requirements.txt
RUN pip3 install  -r /requirements.txt
ADD ./app /app
WORKDIR /app

EXPOSE 80
CMD ["python3", "app.py"]
