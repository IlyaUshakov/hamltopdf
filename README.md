# Haml to Html
Converts Haml file -> Html file. 
Written with Flask framework

Docs generated with Sphinx at \_build/html/index.html

## How it works internally:
Downloaded haml file converts to html file.

### Start service:

Creating image with Docker

	./gradlew HamlToHtmlDocker

Run docker

	./gradlew runDocker

Generate docs with Sphinx to \_build/html

	./gradlew generateDocs


### Send file:
example.haml - file that contacts haml code

To output html

    curl -F 'file=@example.haml' http://localhost:80/convert 

To save as a file

    curl -F 'file=@example.haml' http://localhost:80/convert > index.html